import io from "socket.io";

class SocketStore {
  private sessions: Map<string, io.Socket> = new Map<string, io.Socket>();

  public add(name: string, socket: io.Socket): boolean {
    if (this.sessions.has(name)) return false;
    this.sessions.set(name, socket);

    socket.on("disconnect", () => {
      this.sessions.delete(name);
    });
    return true;
  }

  public get(name: string): io.Socket | undefined {
    if (this.sessions.has(name)) {
      return this.sessions.get(name);
    }

    return undefined;
  }
}

export const socketStore = new SocketStore();
