import { Router, Request, Response, NextFunction } from "express";
import { authorize } from "../passport";
export const router = Router();
import path from "path";
import multer from "multer";
import fs from "fs";
import { store } from "../store";

let storage = multer.diskStorage({
  destination: "./public/uploads",
  filename: function(req, file, cb) {
    cb(
      null,
      `${file.fieldname}_${new Date().getTime()}${path
        .extname(file.originalname)
        .toLowerCase()}`
    );
  }
});

const upload = multer({
  storage: storage,
  fileFilter: checkFileType,
  limits: {
    fileSize: 2000000 //  2MB
  }
}).single("avatar");

const allowedFileTypes = [".jpg", ".jpeg", ".png", ".gif"];

function checkFileType(
  req: Request,
  file: Express.Multer.File,
  cb: (error: Error | null, acceptFile: boolean) => void
) {
  let ext = path.extname(file.originalname).toLowerCase();

  //  TODO: check mimetype cause
  //  users can send application/json?

  if (allowedFileTypes.includes(ext)) cb(null, true);
  else cb(new Error(`wrong file type, stat ${ext}`), false);
}

router.get("/", (req: Request, res: Response, next: NextFunction) => {
  res.render("start");
});

router.get(
  "/chat",
  authorize,
  (req: Request, res: Response, next: NextFunction) => {
    //  @ts-ignore
    res.render("chat", { name: req.user.name });
  }
);

router.post(
  "/chat",
  authorize,
  upload,
  (req: Request, res: Response, next: NextFunction) => {
    //@ts-ignore
    let user = store.get(req.user.id);
    if (!user) return; //  to please typescript

    if (!req.file) {
      res.status(400).render("/chat", { name: user.name });
      return;
    }

    let avatar = req.file;
    let paths = avatar.path.split("/");
    let oldPath = avatar.path;
    paths = paths.slice(0, paths.length - 1);
    paths.push(`${user.name}${path.extname(avatar.originalname)}`);
    avatar.path = paths.join("/");

    try {
      fs.renameSync(oldPath, avatar.path);
    } catch (ex) {
      res.status(500).render("chat", { name: user.name });

      //  TODO: log to sentry/file
      console.log(ex);
      return;
    }

    user.iconUrl = paths.slice(1, paths.length).join("/");
    user.save().then(() => {
      //  @ts-ignore
      res.render("chat", { name: user.name });
    });
  }
);
