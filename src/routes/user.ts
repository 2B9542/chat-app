import { Router, Request, Response, NextFunction } from "express";
import { isNullOrUndefined } from "util";
export const router = Router();
import { User, UserResult } from "../models/User";
import bcrypt from "bcryptjs";
import passport from "passport";

router.get("/register", (req: Request, res: Response, next: NextFunction) => {
  res.render("register");
});

router.post("/register", (req: Request, res: Response, next: NextFunction) => {
  const { name, username, password, confirmPassword } = req.body;
  let errors = new Array<string>();

  //  empty fields
  if (
    isNullOrUndefined(name) ||
    isNullOrUndefined(username) ||
    isNullOrUndefined(password) ||
    isNullOrUndefined(confirmPassword) ||
    name.length == 0 ||
    username.length == 0 ||
    password.length == 0 ||
    confirmPassword.length == 0
  )
    errors.push(`Please fill in every field.`);

  //  password check
  if (password !== confirmPassword) errors.push(`Passwords do not match.`);

  //  TODO: check profanity in name?
  //  TODO: check password length?

  let input = req.body;

  if (errors.length > 0) {
    res.render("register", { errors, input });
  } else {
    let result: UserResult = User.schema.statics.findByName(name);
    if (!isNullOrUndefined(result.error)) {
      //  TODO: log error to file / sentry
      console.error(result.error);
      errors.push(`Registration failed.`);
      res.render("register", { errors, input });
      return;
    }

    if (!isNullOrUndefined(result.user)) {
      //  name taken
      errors.push("Name unavailable.");
      res.render("register", { errors, input });
    } else {
      result = User.schema.statics.findByUsername(username);
      if (!isNullOrUndefined(result.error)) {
        //  TODO: log error to file / sentry
        console.error(result.error);
        errors.push(`Registration failed.`);
        res.render("register", { errors, input });
        return;
      }

      if (!isNullOrUndefined(result.user)) {
        //  username taken
        errors.push("Username unavailable.");
        res.render("register", { errors, input });
      } else {
        result.user = new User({
          name,
          username,
          password
        });

        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(result.user.password, salt);
        result.user.password = hash;

        result.user
          .save()
          .then(() => {
            //  redirect to login
            req.flash("success", "Registration successful.");
            res.redirect("/user/login");
          })
          .catch(reason => {
            //  TODO: log error to file / sentry
            console.log(reason);
            errors.push(`Registration failed.`);
            res.render("register", { errors, input });
          });
      }
    }
  }
});

router.get("/login", (req: Request, res: Response, next: NextFunction) => {
  res.render("login");
});

router.post("/login", (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate("local", {
    successRedirect: "/chat",
    failureRedirect: "/user/login",
    failureFlash: true
  })(req, res, next);
});

router.get("/logout", (req: Request, res: Response, next: NextFunction) => {
  req.logout();
  req.flash("success", "You have been logged out.");
  res.redirect("/user/login");
});
