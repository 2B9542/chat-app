export class Secrets {
  db: {
    username: string;
    password: string;
  } = { username: "", password: "" };
  session: string = "";
}
