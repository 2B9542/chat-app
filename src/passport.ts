import { Strategy as LocalStrategy } from "passport-local";
import bcrypt from "bcryptjs";
import { User, UserResult, UserDocument } from "./models/User";
import { isNullOrUndefined } from "util";
import { PassportStatic } from "passport";
import { NextFunction, Response, Request } from "express";
import { store } from "./store";

export function initialize(passport: PassportStatic) {
  passport.use(
    new LocalStrategy(
      { usernameField: "username" },
      async (username, password, done) => {
        //  TODO: inspect this
        // let result: UserResult = User.schema.statics.findByUsername(username);
        // if (!isNullOrUndefined(result.error)) {
        //   console.log(result.error);
        //   return done(result.error, null);
        // }

        let user = await User.findOne({ username });
        if (isNullOrUndefined(user))
          return done(null, null, { message: "The user doesn't exist." });

        //  check password
        let matches = bcrypt.compareSync(password, user.password);
        if (matches) {
          //  add to store so we can use it in socket.io
          store.add(user.id);
          return done(null, user);
        } else return done(null, null, { message: "Wrong password." });
      }
    )
  );

  passport.serializeUser((user: UserDocument, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => done(err, user));
  });
}

export function authorize(req: Request, res: Response, next: NextFunction) {
  if (req.isAuthenticated()) return next();
  req.flash("error", "This action requires you to be logged in.");
  res.redirect("/user/login");
}
