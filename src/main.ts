let express = require("express");
let app = express();
import mongoose from "mongoose";
import flash from "connect-flash";
import session from "express-session";
import passport from "passport";
import * as layouts from "express-ejs-layouts";
import io from "socket.io";
import { Request, Response, NextFunction } from "express";

import fs from "fs";
import * as path from "path";
import { Server } from "http";
import { isNullOrUndefined } from "util";

import config from "./config.json";
import { Secrets } from "./secrets";
import { store } from "./store.js";

let secrets: Secrets = new Secrets();
if (!fs.existsSync(path.join(__dirname, "secrets.json"))) {
  fs.writeFileSync(
    path.join(__dirname, "secrets.json"),
    JSON.stringify(new Secrets())
  );
  console.log(`secrets.json has been created.`);
  process.exit(1);
} else {
  secrets = require("./secrets.json");
}

let http = new Server(app);
const port = isNullOrUndefined(config.port) ? 4000 : config.port;

//#region Database connection
if (
  isNullOrUndefined(secrets.db) ||
  isNullOrUndefined(secrets.db.username) ||
  isNullOrUndefined(secrets.db.password) ||
  secrets.db.username.length == 0 ||
  secrets.db.password.length == 0
) {
  console.log("invalid database configuration");
  process.exit(1);
}

const connectionString: string = config.connectionString
  .replace("_username_", secrets.db.username)
  .replace("_password_", secrets.db.password);

mongoose
  .connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "chat-app"
  })
  .then(() => console.log("Connected to database."))
  .catch((reason: any) =>
    console.log(`Database connection failed. Reason:\n${reason}`)
  );
//#endregion

app.use(express.static(path.join(__dirname, "../", "public")));

//  passport config
import { initialize } from "./passport";
initialize(passport);

//  view engine
app.use(layouts.default);
app.set("views", path.join(__dirname, "../", "public", "views"));
app.set("view engine", "ejs");

//  body parser
app.use(express.urlencoded({ extended: false }));

//  session
let sessionMw = session({
  secret: secrets.session,
  resave: true,
  saveUninitialized: true
});

let srv = io(http).use((socket, next) => {
  //  @ts-ignore
  sessionMw(socket.request, {}, next);
});

app.use(sessionMw);

//  passport
app.use(passport.initialize());
app.use(passport.session());

//  flash
app.use(flash());
app.use((req: Request, res: Response, next: NextFunction) => {
  res.locals.success = req.flash("success");
  res.locals.error = req.flash("error");
  next();
});

//  routers
import { router as index } from "./routes/index";
import { router as user } from "./routes/user";
import { socketStore } from "./socketStore.js";

app.use("/", index);
app.use("/user", user);

http.listen(port, "0.0.0.0", () => {
  console.log(`Listening on ${port}..`);
});

srv.on("connection", socket => {
  let userID = "";
  try {
    userID = socket.request.session.passport.user;
  } catch {
    //  return if user not authorized
    socket.disconnect(true);
    return;
  }

  socket.on("msg", (message: { content: string; to: string }) => {
    let user = store.get(userID);
    if (!user) {
      socket.emit("error", { message: "Unauthorized." });
      return;
    }

    let fwd = {
      iconUrl: user.iconUrl,
      content: message.content,
      to: message.to,
      from: user.name
    };

    if (fwd.to == "General") srv.emit("msg", fwd);
    else {
      let receiver = socketStore.get(fwd.to);
      if (!receiver) {
        fwd.content = `${fwd.to} is offline`;
        socket.emit("msg", fwd);
        return;
      }

      receiver.emit("msg", fwd);
      socket.emit("msg", fwd);
    }
  });

  let user = store.get(userID);
  if (!user) {
    socket.emit("err", { message: "Unauthorized." });
    return;
  }

  socketStore.add(user.name, socket);

  socket.emit("user-data", { name: user.name, iconUrl: user.iconUrl });
});
