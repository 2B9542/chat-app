import mongoose, { Document } from "mongoose";

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: new Date().getTime()
  },
  iconUrl: {
    type: String,
    required: false,
    default:
      "http://youtopiadesigns.com/wp-content/uploads/2016/06/user_icon.png"
  }
});

UserSchema.static("findByName", async (name: string) => {
  let result: UserResult = { user: null, error: null };

  try {
    result.user = await User.findOne({ name });
  } catch (ex) {
    result.error = ex;
  } finally {
    return result;
  }
});

UserSchema.static("findByUsername", async (username: string) => {
  let result: UserResult = { user: null, error: null };

  try {
    result.user = await User.findOne({ username });
  } catch (ex) {
    result.error = ex;
  } finally {
    return result;
  }
});

export const User = mongoose.model<UserDocument>("User", UserSchema);

export interface UserDocument extends Document {
  name: string;
  username: string;
  password: string;
  createdOn: string;
  iconUrl: string;
}

export interface UserResult {
  user: UserDocument | null;
  error: any;
}
