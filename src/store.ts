import { UserDocument, User } from "./models/User";

class Store {
  private sessions: Map<string, UserDocument> = new Map<string, UserDocument>();

  public async add(userID: string): Promise<boolean> {
    if (this.sessions.has(userID)) return false;

    let user = await User.findById(userID);
    if (!user) return false;

    this.sessions.set(userID, user);
    return true;
  }

  public get(userID: string): UserDocument | undefined {
    if (this.sessions.has(userID)) {
      return this.sessions.get(userID);
    }

    return undefined;
  }
}

export const store = new Store();
