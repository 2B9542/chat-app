# PUBLISH = $1
# OUTPUT = $2

tar -C publish -rf chat_app.tar .
scp chat_app.tar tony@192.168.0.107:~/
rm chat_app.tar

ssh tony@192.168.0.107 << 'ENDSSH'
OUTPUT="chat_app"
RUN="npm run publish"

pm2 stop $OUTPUT
pm2 delete $OUTPUT
rm -rf $OUTPUT
mkdir $OUTPUT
tar -xf $OUTPUT.tar -C $OUTPUT
rm $OUTPUT.tar
cd $OUTPUT
npm i
pm2 start "$RUN" --name $OUTPUT
pm2 save
ENDSSH
