let socket = io("http://pn2sreborn.go.ro");
let messageBox = document.getElementById("messageBox");
let messagesContainer = document.getElementById("messagesContainer");
let channels = document.getElementById("channels");
let userData = {
  name: "",
  iconUrl: ""
};

let chatMessages = new Map();
chatMessages.set("General", document.getElementById("General"));
let activeChatMessages = "General";

window.addEventListener("keydown", ev => {
  if (ev.key == "Enter") sendMessage();
});

socket.on("user-data", data => {
  //  { name: string, iconUrl: string }
  let icon = document.getElementById("user-icon");
  icon.src = userData.iconUrl = data.iconUrl;

  let name = document.getElementById("user-name");
  name.innerText = userData.name = data.name;
});

socket.on("msg", message => {
  //  { content: string, name: string }
  let elem = createMessageElement(message);
  if (message.to != "General") {
    if (message.from == userData.name) {
      //  if we're the sender
      chatMessages.get(message.to).appendChild(elem);
    } else {
      //  we're the receiver
      if (!chatMessages.has(message.from)) createNewChatWindow(message.from);
      chatMessages.get(message.from).appendChild(elem);
    }
  } else {
    //  send to a multi-user channel
    chatMessages.get(message.to).appendChild(elem);
  }

  //  TODO: add a badge displaying the amount of new messages?
});

socket.on("err", error => {
  //  { message: string }
  console.log(error.message);
});

socket.on("user-offline", error => {
  //  { message: string }
  console.log(error.message);
});

function sendMessage() {
  if (messageBox.value.length != 0) {
    let message = { content: messageBox.value, to: "General" };
    if (activeChatMessages != "General") message.to = activeChatMessages;

    socket.emit("msg", message);
    messageBox.value = "";
  }
}

let avatar = document.getElementById("avatar");
avatar.addEventListener("change", () => {
  if (avatar.files[0].size > 2000000) alert("The file is too big.");
});

let generalChannel = document.getElementById("GeneralChannel");
generalChannel.addEventListener("click", () => openChatWindow("General"));

function createMessageElement(message) {
  let username = document.createElement("h5");
  username.innerText = message.from;

  //  we don't wanna dm ourselves
  if (message.from != userData.name) {
    username.style.setProperty("cursor", "pointer");
    username.addEventListener("click", () => {
      //  when we click on username,
      //  we need to create a new chat channel
      //  and a new chat window
      createNewChatWindow(username.innerText);
    });
  }

  let content = document.createElement("p");
  content.innerText = message.content;

  let body = document.createElement("div");
  body.classList.add("media-body");
  body.classList.add("text-break");
  body.appendChild(username);
  body.appendChild(content);

  let icon = document.createElement("img");
  icon.src = message.iconUrl;
  icon.classList.add("mr-3");
  icon.classList.add("img-thumbnail");
  icon.classList.add("rounded");
  icon.classList.add("img-fluid");

  icon.style.setProperty("width", "64px");
  icon.style.setProperty("height", "64px");

  let media = document.createElement("div");
  media.classList.add("media");
  media.appendChild(icon);
  media.appendChild(body);

  let main = document.createElement("li");
  main.classList.add("list-group-item");
  main.appendChild(media);

  return main;
}

function createNewChatWindow(name) {
  let list = document.createElement("ul");
  list.classList.add("list-unstyled");
  list.classList.add("d-none");
  list.id = name;

  messagesContainer.appendChild(list);
  chatMessages.set(name, list);
  createNewChannel(name);
}

function removeChatWindow(name) {
  if (!chatMessages.has(name)) {
    console.error(`chat window ${name} is non-existent`);
    return;
  }

  messagesContainer.removeChild(chatMessages.get(name));
  chatMessages.delete(name);

  //  go back to the general chat
  if (activeChatMessages == name) {
    activeChatMessages = "General";
    generalChannel.dispatchEvent(new MouseEvent("click"));
  }
}

function createNewChannel(name) {
  let div = document.createElement("div");
  div.classList.add("card");
  div.classList.add("d-inline");

  let btn = document.createElement("button");
  btn.type = "button";
  btn.classList.add("btn");
  btn.classList.add("btn-secondary");
  btn.innerText = name;
  btn.addEventListener("click", (self, ev) => {
    //  when we click on the chat channel,
    //  we need to display the chat window
    //  specific to the channel
    openChatWindow(name);
  });

  let closeBtn = document.createElement("button");
  closeBtn.type = "button";
  closeBtn.classList.add("btn");
  closeBtn.classList.add("btn-secondary");
  closeBtn.innerHTML = "&times;";
  closeBtn.addEventListener("click", () => {
    //  remove from channels
    //  remove from windows
    //  remove from dom
    channels.removeChild(div);
    removeChatWindow(name);
  });

  div.appendChild(btn);
  div.appendChild(closeBtn);
  channels.appendChild(div);
}

function openChatWindow(window) {
  chatMessages.get(activeChatMessages).classList.add("d-none");
  activeChatMessages = window;
  chatMessages.get(activeChatMessages).classList.remove("d-none");
}
